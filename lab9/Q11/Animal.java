package Q11;

import java.util.ArrayList;

public abstract class Animal {
     String name;
     String sound;
     ArrayList<Animal> animals = new ArrayList<>();  
	
    public Animal(String name){
    	this.name = name;
    }	
    public String getName() {
    
		return name;
	}

	public abstract String speak();
	
	public void add(Animal name){
		animals.add(name);
	}
}
