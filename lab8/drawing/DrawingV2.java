package drawing;

import java.util.ArrayList;

public class DrawingV2 {
	
	private ArrayList<Object> shapes = new ArrayList<>();
	
	public double totalArea(){
		double totalArea = 0;
		
		for(Object obj: shapes){
			if (obj instanceof Circle){
				Circle circ = (Circle) obj;
			    totalArea += circ.area();
			}else if (obj instanceof Rectangle){
				Rectangle rect = (Rectangle) obj;
				totalArea += rect.area();
			}else if (obj instanceof Square){
				Square sq = (Square) obj;
				totalArea += sq.area();
				
			}
		}
		
		return totalArea;
	}
	
	
	public void draw(){
		for(Object obj: shapes){
			if (obj instanceof Circle){
				Circle circ = (Circle) obj;
			    circ.draw();
			}else if (obj instanceof Rectangle){
				Rectangle rect = (Rectangle) obj;
				rect.draw();
			}else if (obj instanceof Square){
				Square sq = (Square) obj;
				sq.draw();
				
			}
		}	
		
	}

	
	public void move(int xDistance,  int yDistance){
		for(Object obj : shapes){
			
			if(obj instanceof Circle){
				(Circle circle : circles)
					circle.moveCircle(xDistance, yDistance);
			}else if(obj instanceof Rectangle){
			    (Rectangle rect : rectangles)
			    rect.moveRectangle(xDistance, yDistance);
		
			}else if(obj instanceof Square){
			    (Square sq : squares)
			    sq.moveSquare(xDistance, yDistance);
			}    
		}

		for(Rectangle rect: rectangles){
			rect.moveRectangle(xDistance, yDistance);
		}
		
	}
	
	public void addCircle(Circle circle){
		shapes.add(circle);
	}
	
	public void addRectangle(Rectangle rect){
		shapes.add(rect);
	}
	
}
