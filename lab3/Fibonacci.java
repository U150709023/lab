
public class Fibonacci{
 
    public static void main(String[] args){

      int limit = Integer.parseInt(args[0]);
      int element = 0 ;
      System.out.print(element + "," );
      int nextElement = 1;
      System.out.print(nextElement + ",");
      while (element + nextElement < limit){
           int total = element + nextElement;
           System.out.print(total + "," );

           element = nextElement;
           nextElement = total;
      }
      System.out.println();
    }

}
