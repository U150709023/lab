package collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class SearchEngine {

	Map<String, Set<String>> map = new HashMap<>();
	
	    public  void index(String string) throws Exception {

	    	
	    	Document doc = Jsoup.connect(string).get();
	    	
	    	String content = doc.text();
	    	
	    	Pattern p = Pattern.compile("\\b\\w+\\b");
	    	Matcher m = p.matcher(content);
	    	
	    	while(m.find()){
	    		String word = m.group();
	    		Set<String> urls = map.get(word.toLowerCase());
	    		if(urls == null){
	    			urls = new HashSet<String>();
	    			urls.add(string);
	    			map.put(word.toLowerCase(), urls);
	    		}else{
	    			urls.add(string);
	    		}
	    	}
	    	
	    }

		public Set<String> search(String input) {
			Set<String> result = map.get(input.toLowerCase());
			Set<String> noResult = new HashSet<String>();
			noResult.add("No match found!");
			return result != null ? result : noResult;
		}

		public void print() {
			for(Entry<String, Set<String>> entry : map.entrySet())
				System.out.println(entry.getKey() + " = " + entry.getValue());
		}
	


}