package irem.shapes3d;

import irem.shapes.Rectangle;

public class Box extends Rectangle {

	public Box(int sideA, int sideB, int sideC) {
		super(sideA, sideB);
		this.sideC = sideC;
		
	}

	public double area(){
		return 2 * super.area() + 2 * sideA * sideC + 2 * sideB * sideC;
		
	}
	
	public double volume(){
		return super.area() * sideC;
	}
	
}



