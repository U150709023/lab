package irem.shapes3d;

import irem.shapes.Circle;

public class Cylinder extends Circle {

	public Cylinder(int radius, int height) {
		super(radius);
		this.height = height; 
		
	}
    
	public double area(){
		return 2 * super.area() + 2* Math.PI * getRadius();
	}
	
	public double volume(){
		return super.area() * height;
	}
	
	public String toString(){
		return "radius: " + getRadius() + "height: " + height;
	}
	
	
	
	
	
	
	
	
	
	
}
