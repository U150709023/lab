package irem.main;

import irem.shapes.Circle;
import irem.shapes3d.Box;
import irem.shapes3d.Cylinder;


public class Test3D {

	public static void main(String[] args) {
		
           Circle circ = new Circle(5);
           System.out.println("Area of Circle: " + circ.area());
           
           Cylinder cylinder = new Cylinder(5,6);
           System.out.println("Area of Cylinder:" + cylinder.area());
           System.out.println("Volume of Cylinder:" + cylinder.volume());
           
           Circle circle = cylinder;
           
           circle.area();
           
           System.out.println(circle.area());
           
           Object obj = cylinder;
           
           System.out.println(obj.toString());
           
           Box box = new Box(4,5,6);
           System.out.println(box.area());
           System.out.println(box.volume());
        	   
           
           
           
	}
}
